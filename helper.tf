terraform {
  required_version = "0.12.26"
  backend "s3" {
    bucket = "tdsmaug"
    key = "terraform.tfstate"
    region = "us-east-2"
  }
}

provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
  version    = "2.64.0"
}
