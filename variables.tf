variable "access_key" {
  description = "here is my credentials, but I won't show them for you"
  type        = string
}

variable "secret_key" {
  description = "here is my credentials, but I won't show them for you"
  type        = string
}

variable "region" {
  default     = "us-east-2"
  description = "just region"
}

variable "public_key" {
  description = "ssh key"
  type        = string
}
