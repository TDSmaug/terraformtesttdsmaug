#outputing subnet id to use it in "insts" module
output "subnet_id" {
  value = aws_subnet.subnet.id
}

#outputing vpc id to use it in "insts" module
output "vpc_id" {
  value = aws_vpc.vpc.id
}
