locals {

  net_defaults = {

    cidr_block = "192.168.0.0/16"
    all_ranges = "0.0.0.0/0"
    public_ip  = "true"

  }

  subnet_cidr_block = (
    terraform.workspace == "default" ? "192.168.0.0/24"
      : terraform.workspace == "dev" ? "192.168.1.0/24"
        : terraform.workspace == "st" ? "192.168.2.0/24"
          : terraform.workspace == "prod" ? "192.168.3.0/24"
            : null
        )

}
