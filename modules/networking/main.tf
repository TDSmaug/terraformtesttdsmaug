#creating our vpc
resource "aws_vpc" "vpc" {
  cidr_block = local.net_defaults.cidr_block
  tags = {
    Name = "${terraform.workspace}-vpc"
  }
}

#creating internet gateway
resource "aws_internet_gateway" "vpc_gateway" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = "${terraform.workspace}-vpc_gateway"
  }
}

#creating frontend subnet
resource "aws_subnet" "subnet" {
  map_public_ip_on_launch = local.net_defaults.public_ip
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = local.subnet_cidr_block
  tags = {
    Name = "${terraform.workspace}-subnet"
  }
}

#creating route table for frontend subnet, pointing it to the internet gateway
resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = local.net_defaults.all_ranges
    gateway_id = aws_internet_gateway.vpc_gateway.id
  }
  tags = {
    Name = "${terraform.workspace}-route_table"
  }
}

#associating route table with frontend subnet
resource "aws_route_table_association" "association_route_table" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.route_table.id
}
