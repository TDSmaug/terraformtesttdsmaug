#creating key pair
resource "aws_key_pair" "tdsmaug_key" {
  key_name   = "${terraform.workspace}-tdsmaug-key"
  public_key = var.public_key
}

resource "aws_instance" "test_servers" {
  for_each               = terraform.workspace == "default" ? local.ec2_master : local.ec2_nodes
  vpc_security_group_ids = [aws_security_group.sec_group[each.key].id]
  ami                    = each.value.ami
  key_name               = aws_key_pair.tdsmaug_key.key_name
  instance_type          = local.ec2_defaults.instance_type
  subnet_id              = local.ec2_defaults.subnet_id
  tags = {
    Name = "${terraform.workspace}-${each.key}"
  }
  user_data = each.value.user_data
}

resource "aws_security_group" "sec_group" {
  for_each = terraform.workspace == "default" ? local.ec2_master : local.ec2_nodes
  vpc_id   = var.vpc_id
  name     = "${terraform.workspace}-${each.key}-sec-group"
  #rule for having internet
  egress {
    protocol    = "-1"
    cidr_blocks = [local.all_ranges]
    from_port   = 0
    to_port     = 0
  }
  #1448 port as ssh
  ingress {
    protocol    = "tcp"
    cidr_blocks = [local.all_ranges]
    from_port   = 22
    to_port     = 22
  }
  #443 port for apache
  ingress {
    protocol    = "tcp"
    cidr_blocks = [local.all_ranges]
    from_port   = 80
    to_port     = 80
  }

}
