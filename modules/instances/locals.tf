locals {

  ec2_master = {

    master = {
      ami       = "ami-00c03f7f7f2ec15c3"
        user_data = file("./ansible.sh")
    }

  }

  ec2_nodes = {

      amazonI = {
        ami       = "ami-00c03f7f7f2ec15c3"
        user_data = null
      }

      ubuntuII = {
        ami       = "ami-07c1207a9d40bc3bd"
        user_data = null
      }

    }

  ec2_defaults = {

    subnet_id     = var.subnet_id
    instance_type = "t2.micro"

  }

  all_ranges = "0.0.0.0/0"

}
