variable "subnet_id" {
  type        = string
  description = "subnet id"
}

variable "vpc_id" {
  type        = string
  description = "vpc id for creating security groups"
}

variable "public_key" {
  type        = string
  description = "ssh key"
}
