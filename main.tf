#running module "nets"
module "networking" {
  source = "./modules/networking"
}
#running module "insts"
module "insts" {
  source = "./modules/instances"
  #values from module "nets"
  subnet_id  = module.networking.subnet_id
  vpc_id     = module.networking.vpc_id
  public_key = var.public_key
}
